# Automation tools for MPEG workflow

You can use MDMS and GitLab modules in your own scripts. An example how to use them can be found in [examples](./examples) folder or in [systems.py](systems.py).

:warning: It is your responsibility to use the modules with care. Make sure that you make as few requests as possible to MDMS and GitLab servers.

:bulb: New ideas are welcome. Open new issues for your ideas. The general idea is to fire requests to MDMS, process the information, use gitlab API to open issues.

## 1. Requirements

### Environment variables

- `MPEG_LOGIN` - [MDMS](https://dms.mpeg.expert) group login
- `MPEG_PWD` - [MDMS](https://dms.mpeg.expert) group password (changes every meeting)
- `GITLAB_TOKEN` - your private GitLab token with API scope. You can create your token [here](http://mpegx.int-evry.fr/software/-/profile/personal_access_tokens).
  - Give your token a name, check the **api** checkbox and click on **Create personal access token**.
  - Copy your token from the field **Your new personal access token** and add it to `GITLAB_TOKEN` environment variable  
  e.g.: `export GITLAB_TOKEN="abcd123abcxyzwasdfoobar"`

Just in case you are not familiar with Environment Variables you can follow [this Guide](https://www.twilio.com/blog/2017/01/how-to-set-environment-variables.html) to set everything up.

### Python3

It is recommended to use python [virtual environment](https://docs.python.org/3/library/venv.html#module-venv).
Run the following commands to set everything up:

```shell
git clone http://mpegx.int-evry.fr/software/podborski/AutomationTools.git
cd AutomationTools
python3 -m venv venv
source venv/bin/activate (macOS/Linux) or .\venv\Scripts\activate (Windows)
pip install -r requirements.txt

(run your scripts)
(e.g.: python systems.py --csv Contribs.csv -d ... )

deactivate
```

If you don't want to use virtual environment you can just start with: `pip install -r requirements.txt`.

## 2. systems.py

`systems.py` is a script which is intended to be used in the Systems group. Use the `-h` option to see all available parameters.
To provide input data to the script you can either:  
- use `--csv` option to use the CSV file
- or use `-m` (`--documents`) together with `-p` (`--project`) options

If in some cases you see messages like, project not found etc. try using the `-U` option, which will force update all local databases.

Below are a few examples:

1. Open issues based on the information provided in a CSV file:  
e.g.: `python systems.py -o --csv Contribs.csv`
2. Open issues based on CLI options:  
e.g.: `python systems.py -o -m 55958,55959,56121 -p http://... --meeting 133`
3. Generate an output document based on the information provided in a CSV file. Use a template as a basis (`--template` is optional):  
e.g.: `python systems.py -d --csv Contribs.csv --template template.docx`
4. Close issues based on the information provided in a CSV file:  
e.g.: `python systems.py -c --csv Contribs.csv`
5. Close issues based on CLI options:  
e.g.: `python systems.py -c -m m55958,m55959,m56121 -p http://... --meeting 133`
6. Print information about input documents on MDMS and GitLab:  
e.g.: `python systems.py -l -m m55958,m55959,m56121 -p http://... --meeting 133`

The CSV file must have a header row with the folowing entries:

- **Number** - MPEG document numbers
- **Project URL** - a full URL to your GitLab project
- **Close issue** - (optional) if you want to close multiple issues at once. Supported values are `0`, `1`, `TRUE`, `FALSE`, `true` and `false`.

The CSV delimiter is determined automatically. The order of columns does not matter. CSV file example:

```csv
Number;Whatever column;Project URL
m55958;On item encryption;http://mpegx.int-evry.fr/software/MPEG/Systems/FileFormat/CENC
m55959;On multi-key encryption;http://mpegx.int-evry.fr/software/MPEG/Systems/FileFormat/CENC
...
```

## 3. generate_ballot_issues.py

A simple script to generate issues for each ballot comment from a document received from ISO.
