# -*- coding: utf-8 -*-
"""
This is the interface to MPEG GitLab API.
"""

import os
from enum import Enum, unique

import gitlab

BASE_URL = 'https://git.mpeg.expert'
TOKEN = os.environ.get('GITLAB_TOKEN')


@unique
class Label(Enum):
    Accepted = 'Accepted'
    BallotComment = 'BallotComment'
    Combined = 'Combined'
    DocAvailable = 'DocAvailable'
    Editorial = 'Editorial'
    Late = 'Late'
    NeedsRevision = 'NeedsRevision'
    Noted = 'Noted'
    Postponed = 'Postponed'
    ProbableAgreement = 'ProbableAgreement'
    Rejected = 'Rejected'
    Revised = 'Revised'
    SeeDoCR = 'SeeDoCR'
    Withdrawn = 'Withdrawn'


# private token authentication
GL = gitlab.Gitlab(BASE_URL, private_token=TOKEN)

try:
    GL.auth()
    print('GitLab API v{}: Authenticated as "{}"'.format(GL.api_version, GL.user.username))
except gitlab.exceptions.GitlabAuthenticationError:
    print('Error: Could not authenticate. Please set the valid private GITLAB_TOKEN env. variable.')
    GL = None


def _get_project(project_id):
    if not GL:
        print('Error: GitLab API authentication failed.')
        return
    try:
        project = GL.projects.get(project_id)
    except gitlab.exceptions.GitlabGetError as err:
        print('project_id', project_id, err)
        return None
    return project


# --------------------------------------------------------------------------------------------------
# Interfaces
# --------------------------------------------------------------------------------------------------

def get_projects():
    if not GL:
        print('Error: GitLab API authentication failed.')
        return []
    projects = GL.projects.list(all=True)
    projects_stripped = []
    for project in projects:
        projects_stripped.append({
            'id': project.id,
            'name': project.name,
            'url': project.web_url,
            'path_with_namespace': project.path_with_namespace,
            'description': project.description
        })
    return projects_stripped


def get_members(group_id):
    if not GL:
        print('Error: GitLab API authentication failed.')
        return []
    group = GL.groups.get(group_id)
    subgroups = group.subgroups.list()
    members_stripped = {}
    for subgroup in subgroups:
        real_group = GL.groups.get(subgroup.id, lazy=True)
        members = real_group.members.all(all=True)
        for member in members:
            if member.username not in members_stripped:
                members_stripped[member.username] = {
                    'id': member.id,
                    'name': member.name,
                    'url': member.web_url
                }
    return members_stripped


def get_issues(project_id):
    project = _get_project(project_id)
    if not project:
        return []
    issues = project.issues.list(state='opened', all=True)
    return issues


def open_issue(project_id, title, description, labels=None):
    project = _get_project(project_id)
    if not project:
        return
    if labels is None:
        labels = []
    issue = project.issues.create({'title': title, 'description': description, 'labels': labels})
    issue.save()


def close_issue(issue):
    if isinstance(issue, gitlab.v4.objects.ProjectIssue):
        issue.state_event = 'close'
        issue.save()
