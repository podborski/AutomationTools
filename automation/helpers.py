# -*- coding: utf-8 -*-
"""
Some helper functions
"""
import json
import os
import re
from datetime import datetime, timedelta
from automation import mdms, gitlab

from docx import Document, opc, oxml, shared
from docx.enum.text import WD_ALIGN_PARAGRAPH  # pylint: disable=E0611

OPENING_TAG = '[//]: # ( !!! ATTENTION !!! DO NOT MODIFY BEFORE AND AFTER THIS LINE)'
CLOSING_TAG = '[//]: # ( !!! ATTENTION !!! YOU CAN MODIFY AFTER THIS LINE)'

DEADLINE_DAYS = 4  # Number of days to be subtracted from Monday

DISPOSITION_LABELS = ['Accepted', 'Noted', 'Combined', 'Editorial', 'Postponed', 'Rejected', 'Withdrawn']

class DocumentFormatter:
    def __init__(self, template_path):
        self.__doc = Document(docx=template_path)

    def save(self, output_path):
        self.__doc.save(output_path)

    # https://github.com/python-openxml/python-docx/issues/74#issuecomment-261169410
    @staticmethod
    def add_hyperlink(paragraph, url, text):
        part = paragraph.part
        r_id = part.relate_to(url, opc.constants.RELATIONSHIP_TYPE.HYPERLINK, is_external=True)
        hyperlink = oxml.shared.OxmlElement('w:hyperlink')
        hyperlink.set(oxml.shared.qn('r:id'), r_id, )
        run = oxml.shared.OxmlElement('w:r')
        r_pr = oxml.shared.OxmlElement('w:rPr')
        c = oxml.shared.OxmlElement('w:color')
        c.set(oxml.shared.qn('w:val'), '0000EE')
        r_pr.append(c)
        u = oxml.shared.OxmlElement('w:u')
        u.set(oxml.shared.qn('w:val'), 'single')
        r_pr.append(u)
        run.append(r_pr)
        run.text = text
        hyperlink.append(run)
        paragraph._p.append(hyperlink)

    def add_project(self, project):
        if project['description'] is None:
            project_description = ''
        else:
            project_description = project['description'].strip()
        project_url = project['url']
        project_name = project['name']
        h = self.__doc.add_heading('', 2)
        self.add_hyperlink(h, project_url, project_name)
        if len(project_description) > 0:
            p = self.__doc.add_paragraph(project_description)
        else:
            p = self.__doc.add_paragraph('[no project description supplied]')
        p.paragraph_format.keep_with_next = True

    def add_contribution(self, contribution):
        document = contribution['document']
        details = contribution['details']
        issue_meta = contribution['issue_meta']
        issue_title = contribution['issue_title']

        # Create a heading 3 with the document number (linked to a container) and title
        h = self.__doc.add_heading('', 3)
        self.add_hyperlink(h, document['container'], document['document'])
        h.add_run(' ' + document['title'])

        # Create a 4x2 table with all borders
        table = self.__doc.add_table(rows=4, cols=2)
        table.style = 'Table Grid'

        # Set the text of all the cells
        table.rows[0].cells[0].text = 'Authors'
        if details['authors_string'] is not None:
            table.rows[0].cells[1].text = details['authors_string']
        table.rows[0].cells[1].paragraphs[0].alignment = WD_ALIGN_PARAGRAPH.LEFT
        table.rows[1].cells[0].text = 'Abstract'
        if details['abstract'] is not None:
            table.rows[1].cells[1].text = details['abstract']
        table.rows[2].cells[0].text = 'Gitlab'

        issues_added = 0
        if issue_meta is not None:
            self.add_hyperlink(table.rows[2].cells[1].paragraphs[0], issue_meta.web_url, issue_meta.references['full'])
            issues_added += 1
        if issue_title is not None:
            if issues_added > 0:
                p = table.rows[2].cells[1].add_paragraph()
                self.add_hyperlink(p, issue_title.web_url, issue_title.references['full'])
            else:
                self.add_hyperlink(table.rows[2].cells[1].paragraphs[0], issue_title.web_url,
                                   issue_title.references['full'])
            issues_added += 1
        table.rows[3].cells[0].text = 'Disposition'

        if issue_meta:
            for disposition_label in DISPOSITION_LABELS:
                if disposition_label in issue_meta.labels:
                    table.rows[3].cells[1].text = disposition_label
                    break

        # Set column widths
        for cell in table.columns[0].cells:
            cell.width = shared.Cm(2)
        for cell in table.columns[1].cells:
            cell.width = shared.Cm(14)

        return self.__doc


def is_document_late(meeting_start, v1_upload_timestamp):
    """
    meeting_start and v1_upload_timestamp shall be datetime objects
    """
    meeting_start = meeting_start.replace(hour=0, minute=0, second=0)  # paranoia
    deadline = meeting_start - timedelta(days=DEADLINE_DAYS)
    diff = deadline - v1_upload_timestamp
    if diff.total_seconds() <= 0:
        return True
    return False


def try_parsing_date(text):
    """
    Try parsing the timestamp, if not possible return None
    """
    for fmt in ('%Y-%m-%d %H:%M:%S', '%Y-%m-%d'):
        try:
            return datetime.strptime(text.strip(), fmt)
        except ValueError:
            pass
    return None


def load_json_data(json_path):
    """
    Load json file from json_path and return the data.
    """
    with open(json_path, 'r', encoding='utf-8') as f:
        data = json.load(f)
        return data


def store_json_data(json_path, data):
    """
    Store data as a json file to json_path. datetime objects are stored as strings.
    """
    dir_name = os.path.dirname(json_path)
    if not os.path.exists(dir_name) and len(dir_name) > 0:
        os.makedirs(dir_name)
    with open(json_path, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=2, default=str)


def find_meeting(meetings, meeting_number):
    """
    Find and return a meeting using the meeting_number. If meeting_number < 0 return the latest meeting.
    """
    if len(meetings) == 0:
        return None
    if meeting_number < 0:
        return max(meetings, key=lambda x: x['number'])
    for meeting in meetings:
        if meeting['number'] == meeting_number:
            return meeting
    return None


def find_document(documents, document_number):
    striped_doc_nr = document_number.replace(' ', '').strip().lower()
    for doc in documents:
        if striped_doc_nr in doc['document']:
            return doc
    # probably the document is from another meeting, search for it
    found_docs = mdms.find_documents(number=striped_doc_nr.replace('m', ''))
    for doc in found_docs:
        if striped_doc_nr in doc['document']:
            return doc
    return None


def find_project(projects, url_or_path):
    """
    Search for gitlab project based on URL.
    """
    if url_or_path is None:
        return None
    path_with_namespace = url_or_path.replace(' ', '').strip().strip('/')
    path_with_namespace = path_with_namespace.replace('http://mpeg.expert/software/', '')
    path_with_namespace = path_with_namespace.replace('https://mpeg.expert/software/', '')
    path_with_namespace = path_with_namespace.replace('http://mpegx.int-evry.fr/software/', '')
    path_with_namespace = path_with_namespace.replace('https://mpegx.int-evry.fr/software/', '')
    path_with_namespace = path_with_namespace.replace('http://git.mpeg.expert/', '')
    path_with_namespace = path_with_namespace.replace('https://git.mpeg.expert/', '')

    for project in projects:
        if path_with_namespace.lower() == project['path_with_namespace'].lower():
            return project
    return None


def find_issue(issues, document):
    title_only_hit = None
    metadata_hit = None
    last_version = 0
    for issue in issues:
        if document['document'] in issue.title:
            meta = get_issue_metadata(issue.description)
            if meta is None:
                title_only_hit = issue
            else:
                if int(meta['mdms_id']) == document['mdms_id']:
                    metadata_hit = issue
                    if len(meta['version']) > 0:
                        last_version = int(meta['version'])
                else:
                    print('WARNING. We found a GitLab issue with the document number in the title and with metadata '
                          'tag in description. But the metadata tag has wrong document id in it.')
    return title_only_hit, metadata_hit, last_version


def get_issue_metadata(description):
    """
    Find and parse the metada from the description of the issue
    """
    pattern = '[meta]: # ('
    pos1 = description.find(pattern)
    if pos1 < 0:
        return None
    pos2 = description.find(')', pos1 + len(pattern))
    meta_str = description[pos1 + len(pattern):pos2]
    meta = meta_str.split(',')
    if len(meta) != 4:
        return None
    return {'mdms_id': meta[0], 'document': meta[1], 'title': meta[2], 'version': meta[3]}


def create_issue_metadata(document, details):
    """
    Create a metadata tag
    """
    version = ''
    if len(details['documents']) > 0:
        last_doc = max(details['documents'], key=lambda x: x['version'])
        version = str(last_doc['version'])
    title = document['title'].replace('(', '').replace(')', '').replace(',', '')
    meta = '[meta]: # ({},{},{},{})'.format(document['mdms_id'], document['document'], title, version)
    return meta


def create_issue_description_header(document, details):
    """
    Create issue description header, with metadata and the table
    """
    description = OPENING_TAG + '\n'
    description += create_issue_metadata(document, details)
    description += '\n\n| Container | Company | Authors | Document |\n'
    description += '|-|-|-|-|\n'
    description += '| [{}]({}) | {} | '.format(document['document'], document['container'], details['organizations'])
    for author in document['authors']:
        description += ' - {} <br>'.format(author['name'])
    description += ' | '
    for version in details['documents']:
        description += ' - [version {}]({}) <br>'.format(version['version'], version['path'])
    description += ' |\n\n'
    description += CLOSING_TAG
    return description


def create_issue_description(document, details):
    """
    Create the description of the issue: metadata, table, abstract
    """
    description = create_issue_description_header(document, details)

    # request abstract only if submitter is known and not 'Secretariat'
    request_abstract = True
    if details['submitted_by'] is None:
        request_abstract = False
    elif 'secretariat' in details['submitted_by']['name'].lower().strip():
        request_abstract = False

    if details['abstract']:
        description += '\n\n### Abstract\n'
        description += details['abstract']
    elif request_abstract:
        description += '\n\n### Abstract\n'
        description += '* [ ] please **add your abstract here**.\n'
        description += '* [ ] please also **add your abstract to MDMS** (this can be used when we create the output ' \
                       'document).\n '
    description += '\n\n_automatically generated issue_'
    return description


def create_issue_title(document):
    title = document['document'].strip() + ' ' + document['title'].strip()
    if len(title) > 255:
        print('WARNING: The Title of the document {} is too long. GitLab only accepts max of '
              '255 characters'.format(document['document']))
        title = title[0:255]
    return title


def get_updated_issue_description(current_decription, document, details):
    pos1 = current_decription.find(CLOSING_TAG)
    if pos1 < 0:
        return None
    description = create_issue_description_header(document, details)
    description += current_decription[pos1 + len(CLOSING_TAG):]
    return description


def find_gitlab_users(gitlab_users, document):
    usernames = []
    regex = re.compile(r'[^a-zA-Z\s]')
    try:
        for author in document['authors']:
            author_name = author['name'].lower().strip()
            author_name = regex.sub('', author_name)  # remove non alphabetic chars
            author_name = ' '.join([w for w in author_name.split() if len(w) > 1])  # remove single letters
            for key in gitlab_users:
                gl_name = gitlab_users[key]['name'].lower().strip()
                gl_name = regex.sub('', gl_name)  # remove non alphabetic chars
                gl_name = ' '.join([w for w in gl_name.split() if len(w) > 1])  # remove single letters
                if author_name == gl_name:
                    usernames.append(key)
    except:
        return []
    return usernames


def get_contributions(project_url, gitlab_projects, input_docs, days_old=None):
    project = find_project(gitlab_projects, project_url)
    if project is None:
        return None
    issues = gitlab.get_issues(project['id'])

    filtered_issues = None
    if days_old is None:
        filtered_issues = issues
    else:
        limit_date = datetime.now() - timedelta(days=days_old)
        print(f'Look for contributions starting from {limit_date}')
        filtered_issues = [issue for issue in issues if datetime.strptime(issue.created_at, "%Y-%m-%dT%H:%M:%S.%fZ") > limit_date]

    contributions = []
    for issue in filtered_issues:
        meta = get_issue_metadata(issue.description)
        if meta is not None:
            document = find_document(input_docs, meta['document'])
            if not document:
                doc_nr = meta['document'].replace('m', '')
                search_result = mdms.find_documents(number=doc_nr, category=mdms.SearchCategory.INPUT)
                if len(search_result) == 0:
                    print(f'WARNING: Document m{doc_nr} not found.')
                    continue
                else:
                    document = search_result[0]

            contributions.append({
                'project': project,
                'document': document,
                'close': False
                })
    return contributions
