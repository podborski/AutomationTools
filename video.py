import argparse
from docx import shared
from pathlib import Path

from automation import gitlab, helpers, mdms
import systems  # Avoid code duplication


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="A tool for managing the GitLab issues for ISO/IEC JTC 1/SC 29/WG 04 MPEG Video coding activities.",
    )
    parser.add_argument(
        "-i",
        "--input-file",
        help='Input CSV file with a row for each document. The header row shall include "Number" and "Project URL".',
        type=Path,
        required=True,
    )
    parser.add_argument(
        "-o",
        "--open-issues",
        help="Open GitLab issues for new documents and adjust for new versions.",
        action="store_true",
    )
    parser.add_argument(
        "-d",
        "--output-file",
        help="Generate an AHG or BoG report.",
        type=Path,
    )
    parser.add_argument(
        "-t",
        "--template-file",
        help="Document template file path to be used as a basis for the report.",
        type=Path,
    )
    parser.add_argument(
        "-f",
        "--fetch-contributions",
        help="Download the latest version of each contribution.",
        action="store_true",
    )
    parser.add_argument(
        "-z",
        "--unzip-contributions-dir",
        help="Unzip fetched contributions in this directory",
        type=Path,
    )
    parser.add_argument(
        "-u",
        "--update-all-databases",
        help="Update the local databases.",
        action="store_true",
    )
    parser.add_argument(
        "--meeting-id",
        help="MPEG meeting number. If not set, the latest meeting is selected.",
        default=-1,
        type=int,
    )
    return parser.parse_args()


DATA_PATH = Path("data")
GITLAB_PROJECTS_PATH = DATA_PATH / "gitlab_projects.json"
GITLAB_USERS_PATH = DATA_PATH / "gitlab_users.json"
MEETINGS_PATH = DATA_PATH / "meetings.json"
VIDEO_GROUP_ID = 901  # GitLab Group ID for MPEG Video coding


def main(
    open_issues: bool,
    fetch_contributions: bool,
    unzip_contributions_dir: Path,
    update_all_databases: bool,
    input_file: Path,
    output_file: Path,
    template_file: Path,
    meeting_id: int,
):
    if update_all_databases or not GITLAB_PROJECTS_PATH.is_file():
        print("Updating GitLab projects data.")
        projects = gitlab.get_projects()
        helpers.store_json_data(GITLAB_PROJECTS_PATH, projects)

    if update_all_databases or not GITLAB_USERS_PATH.is_file():
        print("Updating GitLab users data.")
        members = gitlab.get_members(VIDEO_GROUP_ID)
        helpers.store_json_data(GITLAB_USERS_PATH, members)

    if update_all_databases or not MEETINGS_PATH.is_file():
        print("Updating MPEG meetings data.")
        meetings = mdms.get_meetings()
        helpers.store_json_data(MEETINGS_PATH, meetings)

    gitlab_projects = helpers.load_json_data(GITLAB_PROJECTS_PATH)
    gitlab_members = helpers.load_json_data(GITLAB_USERS_PATH)
    meetings = helpers.load_json_data(MEETINGS_PATH)
    meeting = helpers.find_meeting(meetings, meeting_id)

    if not meeting:
        raise RuntimeError(f"Could not find meeting #{meeting_id}.")
    print(
        f"Operating on MPEG {meeting['number']} ({meeting['name']}) "
        f"from {meeting['start_date']} to {meeting['end_date']}."
    )

    print("Updating MPEG input documents data.")
    input_docs = mdms.get_input_documents(meeting["id"])

    if not input_docs:
        raise RuntimeError("Could not find any input documents from the MDMS.")

    helpers.store_json_data(
        DATA_PATH / f"input_docs_{meeting['number']}.json", input_docs
    )
    table_entries = systems.parse_csv(input_file, gitlab_projects, input_docs)

    if open_issues:
        meeting_start = helpers.try_parsing_date(meeting["start_date"])
        systems.open_issues(table_entries, False, gitlab_members, meeting_start)

    if output_file:
        generate_meeting_report(table_entries, output_file, template_file)

    if fetch_contributions:
        systems.fetch_contributions(table_entries, unzip_contributions_dir)


def generate_meeting_report(
    table_entries: list[dict], output_file: Path, template_file: Path
):
    print(f"Generating meeting report: {output_file}.")
    projects, projects_data = collect_data_for_meeting_report(table_entries)

    formatter = helpers.DocumentFormatter(template_file)

    for project_id in projects_data:
        formatter.add_project(projects[project_id])

        for contribution in projects_data[project_id]:
            doc = formatter.add_contribution(contribution)

            have_minutes = False
            issue_meta = contribution["issue_meta"]

            if issue_meta:
                for discussion in issue_meta.discussions.list():
                    for note in discussion.attributes["notes"]:
                        if note["body"].startswith("# "):
                            lines = note["body"].split("\n")
                            title = lines[0][1:].strip()
                            minutes = "\n".join(lines[1:]).strip()
                            date = note["created_at"][:10]

                            h = doc.add_heading(f"{title} ({date})", 4)
                            p = doc.add_paragraph(minutes)
                            have_minutes = True

            if not have_minutes:
                p = doc.add_paragraph("There are no minutes.")
                p.paragraph_format.space_before = shared.Pt(8)

            if issue_meta and issue_meta.labels:
                p = doc.add_paragraph(f"Labels: {', '.join(issue_meta.labels)}")
                p.paragraph_format.space_before = shared.Pt(8)
            else:
                p = doc.add_paragraph("There are no labels.")

    formatter.save(output_file)


def collect_data_for_meeting_report(table_entries: list[dict]):
    projects = {}
    projects_data = {}

    for entry in table_entries:
        document = entry["document"]
        details = mdms.get_document_details(document["mdms_id"])

        if details is None:
            print(f" * Skipping: {document['document']}.")
        else:
            print(f" * Reporting: {document['document']} {document['title']}.")

            project = entry["project"]
            project["name"] = project["path_with_namespace"]  # More descriptive
            project_id = project["id"]
            projects[project_id] = project

            issues = gitlab.get_issues(project_id)
            issue_with_title, issue_with_meta, _ = helpers.find_issue(issues, document)

            if not project_id in projects_data:
                projects_data[project_id] = []

            projects_data[project_id].append(
                {
                    "document": document,
                    "details": details,
                    "issue_meta": issue_with_meta,
                    "issue_title": issue_with_title,
                }
            )

    return projects, projects_data


if __name__ == "__main__":
    try:
        main(**vars(parse_arguments()))
    except RuntimeError as e:
        print(f"ERROR: {e}")
        exit(1)
