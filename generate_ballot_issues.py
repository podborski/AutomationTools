import os
import gitlab
from docx import Document
from enum import Enum, unique

PROJECT_ID = 321 # this ID is my private repo for testing. The ID can be found on the main project webpage
BALLOT_DOC = 'ISO_IEC DIS 23090-10 Collated Comments.doc'

@unique
class ColumType(Enum):
    NC = 0
    LINE_NUMBER = 1
    CLAUSE = 2
    PARAGRAPH = 3
    TYPE = 4
    COMMENT = 5
    PROPOSAL = 6


def get_gitlab():
    # constants
    TOKEN = os.environ.get('GITLAB_TOKEN')

    if not TOKEN:
        print("No private token found, please set the GITLAB_TOKEN environment variable")
        sys.exit(1)
    
    # private token authentication
    return gitlab.Gitlab('https://git.mpeg.expert', private_token=TOKEN)

def open_issue(gl, project_id, title, description, labels=[]):
    project = gl.projects.get(project_id)
    issue = project.issues.create({'title': title, 'description': description, 'labels': labels})
    issue.save()

if __name__ == "__main__":

    gl = get_gitlab()
   
    document = Document(BALLOT_DOC)

    for table in document.tables:
        for r, row in enumerate(table.rows):
            title = ""
            description = ""
            labels = [ "BallotComment" ]
            for c, column in enumerate(row.cells):
                if (c == ColumType.NC.value):
                    title += str(table.cell(r,c).text.rstrip())
                elif (c == ColumType.LINE_NUMBER.value):
                    description += "Line: " + str(table.cell(r,c).text.rstrip()) + str("\n\n")
                elif (c == ColumType.CLAUSE.value):
                    title += " clause " + str(table.cell(r,c).text)
                elif (c == ColumType.PARAGRAPH.value):
                    description += "Paragraph: " + str(table.cell(r,c).text.rstrip()) + str("\n\n")
                elif (c == ColumType.TYPE.value):
                    if(str(table.cell(r,c).text).lower().find("te") != -1):
                        labels.append("Technical")
                    if(str(table.cell(r,c).text).lower().find("ge") != -1):
                        labels.append("General")
                    if(str(table.cell(r,c).text).lower().find("ed") != -1):
                        labels.append("Editorial")
                elif (c == ColumType.COMMENT.value):
                    description += "Comment: \n > " + str(table.cell(r,c).text.rstrip()) + str("\n\n")
                elif (c == ColumType.PROPOSAL.value):
                    description += "Proposal: \n > " + str(table.cell(r,c).text.rstrip()) + str("\n\n")

            description += "\n_automatically generated issue_"
            open_issue(gl, PROJECT_ID, title, description, labels)
              