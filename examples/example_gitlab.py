"""
Just a few examples on how to use gitlab module
"""
import sys

sys.path.append('..')  # hack which allows to import module from the parent directory
from automation import gitlab

# get all projects
projects = gitlab.get_projects()
print('project count:', len(projects))
for project in projects:
    print(project)

issues = gitlab.get_issues(projects[0]['id'])
print('project {} has {} issues.'.format(projects[0]['url'], len(issues)))
