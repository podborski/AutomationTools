"""
Just a few examples on how to use mdms module
"""
import sys

sys.path.append('..')  # hack which allows to import module from the parent directory
from automation import mdms

# Get all meetings
meetings = mdms.get_meetings()
print('Number of MPEG meetings:', len(meetings))

# Get latest meeting, (this calles mdms.get_meetings() internally)
last_meeting = mdms.get_current_meeting()
print('\nLast MPEG#{} ({}) from {} to {}'.format(last_meeting['number'], last_meeting['name'],
                                                 last_meeting['start_date'], last_meeting['end_date']))

# Get all input documents of a certain meeting
input_docs = mdms.get_input_documents(last_meeting['id'])
print('\nNumber of input contributions:', len(input_docs))
print('First entry:', input_docs[0])

# Get detailed information of a certain document
doc_details = mdms.get_document_details(input_docs[0]['mdms_id'])
print('\nDetails of the first document:', doc_details)
