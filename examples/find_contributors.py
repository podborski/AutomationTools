'''
This is an example how to get a list of authors who had input documents 
with a specific substring in the title
'''
import sys
sys.path.append('..') # hack which allows to import module from the parent directory
from automation import mdms, helpers
import os

# meetings with the smaller number will be ignored
LAST_MEETING_NUMBER = 42
DATA_PATH = './data'
CONRIBUTORS_PATH = os.path.join(DATA_PATH, 'ff_contributors.json')
TITLE_WHITELIST= ['ISOBMFF', 'BMFF', '14496-12', 'file format', 'MP4', 'ISOBMF', 'ISO Base']

def document_is_relevant(title, filter_strings):
  return any(sub.lower().strip() in title.lower().strip() for sub in filter_strings)

def get_contributors(last_meeting_number):
  authors = {}
  meetings = mdms.get_meetings()
  for meeting in meetings:
    if meeting['number'] < last_meeting_number:
      continue
    print('process meegting', meeting['number'])
    input_docs = mdms.get_input_documents(meeting['id'])
    for doc in input_docs:
      if not document_is_relevant(doc['title'], TITLE_WHITELIST):
        continue
      for author in doc['authors']:
        if author['name'] in authors:
          authors[author['name']].append(doc)
        else:
          authors[author['name']] = [doc]
  return authors

if not os.path.isfile(CONRIBUTORS_PATH):
  authors = get_contributors(LAST_MEETING_NUMBER)
  helpers.store_json_data(CONRIBUTORS_PATH, authors)
authors = helpers.load_json_data(CONRIBUTORS_PATH)


blacklist = ['SC 29 Secretariat', 'WG 1', 'ITTF via SC 29 Secretariat', 'ITTF', 'ISO secretariat']
groups = {}
dups = ['Singer', 'Hannuksela', 'Aksu', 'Sreedhar', 'Zia', 'Stockhammer', 'Yago', 'Feuvre', 'Deshpande', 'Curcio']
for name in authors:
  if name in blacklist or len(name) == 0:
    continue
  skip = False
  for k in groups:
    for dup in dups:
      if dup in name and dup in k:
        groups[k] += len(authors[name])
        skip = True
        break
    if skip:
      break
  if not skip:
    groups[name] = len(authors[name])

groups = {key:val for key, val in groups.items() if val > 10}
groups = dict(sorted(groups.items(), reverse=True, key=lambda item: item[1]))

output = {
  'authors': [],
  'contributions': []
}
for entry in groups:
  output['authors'].append(entry)
  output['contributions'].append(groups[entry])
  print(entry, ' ', groups[entry])
helpers.store_json_data('./data/plot.json', output)