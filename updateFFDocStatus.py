"""
Script to link output doc# from http://mpegx.int-evry.fr/software/MPEG/Systems/FileFormat/General/-/blob/master/DocumentStatus.md to MDMS
"""

from automation import gitlab, mdms

PROJECT_ID = '195' # General project ID

def add_urls(input_str):
    lines = input_str.split('\n')
    lines_out = []
    for line in lines:
        cols = line.split('|')
        if len(cols) < 7:
            lines_out.append(line)
            continue
        try:
            doc_nr = int(cols[6])
        except ValueError:
            lines_out.append(line)
            continue

        print('processing', doc_nr)
        docs = mdms.find_documents(category=mdms.SearchCategory.OUTPUT, number=str(doc_nr))

        if len(docs) == 0:
            print('WARNING document could not be found', doc_nr)
            lines_out.append(line)
            continue
        elif len(docs) > 1:
            print('WARNING document could not be found', doc_nr)
            lines_out.append(line)
            continue
        
        cols[6] = '[' + str(doc_nr) + '](' + docs[0]['container'] + ')'
        line_out = '|'.join(cols)
        lines_out.append(line_out)
    return '\n'.join(lines_out)

def main():
    print('*' * 35)
    print('* Update FileFormat DocumentStatus.md *')
    print('*' * 35 + '\n')

    project = gitlab._get_project(PROJECT_ID)

    input_str = project.files.raw(file_path='DocumentStatus.md', ref='master').decode('utf-8')

    output_str = add_urls(input_str)
    with open('DocumentStatus.md', 'w') as f:
        f.write(output_str)

if __name__ == "__main__":
    main()
